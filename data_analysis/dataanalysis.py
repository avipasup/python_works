import pandas as pd
franchisedata=pd.read_excel('data/mis20816/franchise_record_report190816.xlsx')
pendingdata=pd.read_excel('data/mis20816/pending_status_report19816.xlsx')
holddata=pd.read_excel('data/mis20816/hold_samples.xlsx')
rejectdata=pd.read_excel('data/mis20816/reject_samples19816.xlsx')
positivdata=pd.read_excel('data/mis20816/positive_communication16816.xlsx')
pincodedata=pd.read_csv('data/mis20816/all_india_pin_code.csv',encoding='utf-8')

#rdata[[1,3]][0:8]

fdata=franchisedata[[0,1,4,5,6,8,9,10,11,12,16,17,19,22,27,33,34,35]]
pendata=pendingdata[[0,1,2,7,8,9,10,15,18,21,22]]
hdata=holddata[[1,4,5,7,8,]]
rdata=rejectdata[[0,1,5,8,9,10,14,15,16,17,18,20,23,25,28,31,32]]
posdata=positivdata[[0,1,4,5,7,8,9,11,15,16,17,18,21,24,26,29,33]]

rtempdata=pd.DataFrame(columns=fdata.columns)
for i in range(1,len(rdata.index)-1):
    for j in range(1,len(fdata.index)-1):
        if rdata.values[i][1]==fdata.values[j][1]:
            rtempdata.loc[len(rtempdata.index)]=fdata.loc[j]

