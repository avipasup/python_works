import pandas as pd
import matplotlib.pyplot as plt
import Bio
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_dna

#importing *.ab1 files

for seq85 in SeqIO.parse("S2997_85_E11_009.ab1","abi"):
    print(seq85)
    seq85a=SeqRecord(Seq(str(seq85.seq),generic_dna),id="fasta|85|testseq",
    description="This is a test sequence from thesis seq")

for seq86 in SeqIO.parse("S2997_86_F11_011.ab1","abi"):
    seq86a=SeqRecord(Seq(str(seq86.seq),generic_dna),id="fasta|86|testseq",
    description="This is a test sequence from thesis seq")
    
for seq87 in SeqIO.parse("S2997_87_G11_013.ab1","abi"):
    seq87a=SeqRecord(Seq(str(seq87.seq),generic_dna),id="fasta|87|testseq",
    description="This is a test sequence from thesis seq")
    
for seq88 in SeqIO.parse("S2997_88_H11_015.ab1","abi"):
    seq88a=SeqRecord(Seq(str(seq88.seq),generic_dna),id="fasta|88|testseq",
    description="This is a test sequence from thesis seq")
    
for seq89 in SeqIO.parse("S2997_89_A12_002.ab1","abi"):
    seq89a=SeqRecord(Seq(str(seq89.seq),generic_dna),id="fasta|89|testseq",
    description="This is a test sequence from thesis seq")

testseq=[seq85a,seq86a,seq87a,seq88a,seq89a]
SeqIO.write(testseq,"testseq.fasta","fasta")


from Bio import AlignIO
seqtest=AlignIO.read(open("S2997_85_E11_009.ab1"))
,"S2997_86_F11_011.ab1","S2997_87_G11_013.ab1",
"S2997_88_H11_015.ab1")


#Calculating GC%
k=0
for i in range(0,(len(seq)-1)):
    if (seq[i]=="C") or (seq[i]=="G"):
        k+=1
    gcratio=(k/len(seq))*100
print("The GC content is ",k," in 1170 bp")
print("The GC ratio in the test seq is ",gcratio,"%")

#writing seq data to fasta format
SeqIO.write(seq,"test.fasta","fasta")
            #or
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_dna
testrec=SeqRecord(Seq("CAATCCTCATCTTAATACCTCGCATTTGATAAAACATATAT"\
+"GTTCAGGCAAATTTTTTAGATGCAGACAGCCTTACTGCTACCGGAGCAGTTAACTGAAGCCAGC"\
+"TTGCGTGCGATGACCTGAACGTCGTCCTGTTGGCTTAACCAGGCCTGCTCAATAATCTGGGCAG"\
+"CCCATGAAGGAATATGCGGTGATAAGCGGTAGTGAACCCATTTTCCCTGTTTACGATCCAGAAG"\
+"GATTCCACTTTCCCGTAGCATCGCCAGATGACGGGATATTTTGGGCTGTGATTGATCCAGTGCC"\
+"ATGCAAAGATCACACACGCACAACTCTCCCATCTCCCTGAGCAACAACACGATACCCAAACGGG"\
+"TTTCATCGGACAGGTTTTTAAATAACTGAAGTGGTGTTAGTTGCAACATTTCCCTCTCCTTTGT"\
+"AGAAAACAAGTATCTCTCGGCTCTGATATTAAGTCAAACAACACATAACCAAAAACGCATATGA"\
+"TTAATCACACTATTACCTTAAGGTAGGTGAGATAACTTGCTCGACTGCATTAATGAATCGGCCA"\
+"ACGCGCGGGGAGAGGCGGTTTGCGTATTGGGCGCTCTTCCGCTTCCTCGCTCACTGACTCGCTG"\
+"CGCTCGGTCGTTCGGCTGCGGCGAGCGGTATCAGCTCACTCAAAGGCGGTAATACGGTTATCCA"\
+"CAGAATCAGGGGATAACGCAGGAAAGAACATGTGAGCAAAAGGCCAGCAAAAGGCCAGGAACCG"\
+"TAAAAAGGCCGCGTTGCTGGCGTTTTTCCATAGGCTCCGCCCCCCTGACGAGCATCACAAAAAT"\
+"CGACGCTCAAGTCAGAGGTGGCGAAACCCGACAGGACTATAAAGATACCAGGCGTTTCCCCCTG"\
+"GAAGCTCCCTCGTGCGCTCTCCTGTTCCGACCCTGCCGCTTACCGGATACCTGTCCGCCTTTCT"\
+"CCCTTCGGGAAGCGTGGCGCTTTCTCATAGCTCACGCTGTAGGTATCTCAGTTCGGTGTAGTCG"\
+"TTCGCTCAAGCTGGGCTGTGTGCACGAACCCCCCGTTCAGCCCGACCGCTGCGCCTATCAGTAC"\
+"TATCGTCTGAGTCACCGTTAGGACACGACTATCGGCACTGCAGCAGCACTGTACAGAATAGCTG"\
+"GAGCAGTATGTAGCCGGTCTACAGAGTTCTTTGAAGTTTTC",generic_dna),id="fasta|85|testseq",
description="This is a test sequence from thesis seq")

SeqIO.write(testrec,"test.fasta","fasta")

#get phred score and assign to dataframe
sequence=list(range(0,len(seq85.seq)))
print(seq85.letter_annotations["phred_quality"])
phred={"phred_score":seq85.letter_annotations["phred_quality"],
        "num":sequence}

phreddata=pd.DataFrame(phred)
plt.plot(phreddata)
plt.axhline(20,0,1200,color='r',linewidth=2)
plt.ylim((0,100))
plt.ylabel("Phred Score")
plt.xlabel("Sequence")
plt.show()
Bio.S


